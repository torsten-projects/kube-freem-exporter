FROM docker.io/library/golang:1.18-alpine AS build
WORKDIR /build
RUN apk add --no-cache git
COPY . .
RUN CGO_ENABLED=0 go build -v -installsuffix 'static' ./cmd/exporter

FROM scratch AS runtime
ENTRYPOINT [ "/exporter" ]
COPY --from=build /build/exporter /
