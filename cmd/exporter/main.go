// Package main is the entry point for this Go application
package main

import (
	// Import the context package to manage cancellation and timeouts
	"context"
	// Import the fmt package to format and print output
	"fmt"
	// Import the log package for logging messages
	"log"
	// Import the net/http package for creating an HTTP server
	"net/http"
	// Import the time package for working with time values
	"time"
	// Import the Prometheus Go client package
	"github.com/prometheus/client_golang/prometheus"
	// Import the Prometheus HTTP handler package
	"github.com/prometheus/client_golang/prometheus/promhttp"
	// Import the slices package from the Go experimental modules
	"golang.org/x/exp/slices"
	// Import the Kubernetes Batch v1 API
	batch_v1 "k8s.io/api/batch/v1"
	// Import the Kubernetes metadata API
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	// Import the Kubernetes Go client
	"k8s.io/client-go/kubernetes"
	// Import the Kubernetes Go client rest package
	"k8s.io/client-go/rest"
	// Import the Kubernetes Go client tools package
	"k8s.io/client-go/tools/clientcmd"
	// Import the Kubernetes Go client home directory package
	"k8s.io/client-go/util/homedir"
)

// check is a helper function that checks for errors and panics if one is found
func check(err error) {
	if err != nil {
		panic(err)
	}
}

// JobResult holds the result of a job execution
type JobResult struct {
	owner      CronjobReference
	startTime  time.Time
	successful bool
}

// CronjobReference holds the namespace and name of a cronjob
type CronjobReference struct {
	namespace string
	name      string
}

// populateCronjobStates populates a collector with the current state of cronjobs
func populateCronjobStates(client *kubernetes.Clientset, collector *collector, metric_channel chan<- prometheus.Metric) {
	// Set initial values for counters to track API call duration
	fetch_namespace_total := 0.0
	fetch_jobs_total := 0.0
	fetch_cronjobs_total := 0.0
	// Start a timer to track the duration of the List Namespaces API call
	start := time.Now()
	// Use the Kubernetes client to list all namespaces
	namespace_list, err := client.CoreV1().Namespaces().List(context.TODO(), metav1.ListOptions{})
	// Add the duration of the List Namespaces API call to the counter
	fetch_namespace_total += time.Since(start).Seconds()
	// Check for errors and panic if one is found
	check(err)
	// Get the list of namespaces from the response
	namespaces := namespace_list.Items
	// Start a timer to track the duration of the List CronJobs API call
	start = time.Now()
	// Use the Kubernetes client to list all cronjobs in the default namespace
	cronjob_list, err := client.BatchV1().CronJobs("").List(context.TODO(), metav1.ListOptions{})
	// Add the duration of the List CronJobs API call to the counter
	fetch_cronjobs_total += time.Since(start).Seconds()
	// Get the list of cronjobs from the response
	cronjobs := cronjob_list.Items
	// Initialize a slice to hold the results of job executions
	job_results := make([]JobResult, 0)
	// Iterate over each namespace in the list of namespaces
	for _, namespace_obj := range namespaces {
		// Start a timer to track the duration of the List Jobs API call
		start = time.Now()
		// Use the Kubernetes client to list all jobs in the current namespace
		job_list, err := client.BatchV1().Jobs(namespace_obj.Name).List(context.TODO(), metav1.ListOptions{})
		// Add the duration of the List Jobs API call to the counter
		fetch_jobs_total += time.Since(start).Seconds()
		// Check for errors and panic if one is found
		check(err)
		// Get the list of jobs from the response
		jobs := job_list.Items
		// Iterate over each job in the list of jobs
		for _, job := range jobs {
			// Get the namespace of the current job
			namespace := job.Namespace
			// Get the owner reference for the current job
			owner_reference := job.OwnerReferences
			// If the job has no owner reference, log a message and skip to the next job
			if len(owner_reference) == 0 {
				log.Printf("Job %v in namespace %v has no owner reference, ignoring", job.Name, namespace)
				continue
			}
			// Get the name of the owner cronjob
			cronjob := owner_reference[0].Name
			// Get the status of the current job
			status := job.Status
			// If the job has not started yet, log a message and skip to the next job
			if status.StartTime == nil {
				log.Printf("Job %v in namespace %v didn't run yet, ignoring", job.Name, namespace)
				continue
			}
			// Get the start time of the current job
			time := status.StartTime.Time
			// If the job is still running, log a message and skip to the next job
			if status.Active > 0 {
				log.Printf("Job %v in namespace %v is still running, ignoring", job.Name, namespace)
				continue
			}
			// Determine if the job execution was successful
			successful := status.Succeeded > 0

			if !successful {
				// first, check if the owner cronjob is not suspended
				cronjob_ix := slices.IndexFunc(cronjobs, func(cj batch_v1.CronJob) bool { return cj.Namespace == namespace && cj.Name == cronjob }) // Find the index of the cronjob in the list of cronjobs with the specified namespace and name
				if cronjob_ix < 0 {
					log.Printf("Cronjob %v in namespace %v could not be located and checked for suspension", cronjob, namespace) // If the cronjob could not be found, log a message
				} else {
					cronjob := cronjobs[cronjob_ix] // Get the cronjob object
					if *cronjob.Spec.Suspend {      // Check if the cronjob is suspended
						log.Printf("Job %v in namespace %v failed, but cronjob is suspended, ignoring", job.Name, namespace) // If the cronjob is suspended, log a message
						continue                                                                                             // Go to the next iteration of the loop
					}
				}
			}

			owner := CronjobReference{ // Create a CronjobReference object representing the cronjob that owns the job
				namespace: namespace, // Set the namespace
				name:      cronjob,   // Set the name
			}

			new_result := JobResult{ // Create a JobResult object representing the result of the job execution
				owner:      owner,      // Set the owner cronjob
				startTime:  time,       // Set the start time of the job
				successful: successful, // Set whether the job was successful
			}

			// check if job result already present
			should_insert := true                     // Set a flag indicating whether the new JobResult should be inserted
			for ix, job_result := range job_results { // Loop through the existing JobResult objects
				if job_result.owner == owner { // If the owner cronjob of the existing job result is the same as the new job result
					// same owner
					should_insert = false                                    // Set the flag indicating that the new JobResult should not be inserted
					if job_result.startTime.UnixMilli() > time.UnixMilli() { // If the existing job result has a newer start time than the new job result
						// present job is already newer than this one, ignore this one
						continue // Go to the next iteration of the loop
					} else {
						// present job is older than this one, replace
						job_results[ix] = new_result // Replace the existing job result with the new one
					}
				}
			}

			if should_insert { // If the new JobResult should be inserted
				job_results = append(job_results, new_result) // Append the new JobResult to the list of job results
			}
		}
	}

	log.Printf("Writing %v cronjob failed state metrics", len(job_results))
	log.Printf("%v s spent gathering namespaces", fetch_namespace_total)
	log.Printf("%v s spent gathering jobs", fetch_jobs_total)
	log.Printf("%v s spent gathering cronjobs", fetch_cronjobs_total)

	// loop through each job result
	for _, job := range job_results {
		// initialize failure_indicator to 0
		failure_indicator := 0
		// if the job was not successful, set failure_indicator to 1
		if !job.successful {
			failure_indicator = 1
		}
		// add the failure_indicator metric to the metric_channel with the appropriate labels
		metric_channel <- prometheus.MustNewConstMetric(collector.cronjobFailed, prometheus.GaugeValue, float64(failure_indicator), job.owner.namespace, job.owner.name)
	}

}

type collector struct {
	cronjobFailed *prometheus.Desc
}

func newCollector() *collector {
	metrics_namespace := "kube_freem_exporter"
	return &collector{
		cronjobFailed: prometheus.NewDesc(
			fmt.Sprintf("%v_cronjob_failed", metrics_namespace),
			"Indicates whether the last run of a non-suspended cronjob failed",
			[]string{
				"namespace",
				"cronjob",
			},
			nil,
		),
	}
}

func getClient() *kubernetes.Clientset {
	config, err := rest.InClusterConfig()
	if err != nil {
		log.Print("Unable to read in-cluster kubeconfig, trying ~/.kube/config")
		config, err = clientcmd.BuildConfigFromFlags("", fmt.Sprintf("%v/.kube/config", homedir.HomeDir()))
		check(err)
	}
	clientset, err := kubernetes.NewForConfig(config)
	check(err)
	return clientset
}

var client *kubernetes.Clientset = getClient()

func (collector *collector) Describe(ch chan<- *prometheus.Desc) {
	ch <- collector.cronjobFailed
}

func (collector *collector) Collect(ch chan<- prometheus.Metric) {
	populateCronjobStates(client, collector, ch)
}

func main() {

	collector := newCollector()
	prometheus.MustRegister(collector)
	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/ready", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprintf(w, "Ready")
	})
	log.Println("Starting exporter")
	http.ListenAndServe(":8000", nil)
}
