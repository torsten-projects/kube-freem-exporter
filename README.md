# Overview

[![Build Status](https://gitlab.com/torsten-projects/kube-freem-exporter/badges/master/pipeline.svg)](https://gitlab.com/torsten-projects/kube-freem-exporter/badges/master/pipeline.svg)

kube-freem-metrics is a simple exporter that adds features missing from [kube-state-metrics](https://github.com/kubernetes/kube-state-metrics) (KSM).

# Exported metrics
# `kube_freem_exporter_cronjob_failed`
## Description
Exposes the failure state of a cronjob, being 1 when the latest finished run of the target cronjob has not succeeded.
## Labels
- `namespace`: the namespace of the target cronjob
- `cronjob`: the name of the target cronjob
## Values
- `1` if the cronjob has failed
- `0` if the cronjob has not failed (not necessarily successful)

Note: the value can also be `0` if the cronjob is suspended or if none of its pods have finished running due to scheduling issues (this can already be tracked using KSM).

# Required RBAC permissions
This exporter requires the following permissions:
| apiGroup 	| resource 	| verb 	|
|----------	|----------	|------	|
| batch    	| cronjobs 	| get  	|
| batch    	| cronjobs 	| list 	|
| batch    	| jobs     	| get  	|
| batch    	| jobs     	| list 	|

# Exposed paths
## `/metrics`
Prometheus metric endpoint. Scraping data occurs on request, so a larger scrape timeout may be required on larger clusters.
## `/ready`
Readiness + liveness endpoint. Always returns `Ready` with a `200` status code as long as the HTTP server is accepting connections.

# Kubernetes version compatibility
This project was started with Kubernetes version 0.24.2 and compatibility with previous versions is not guaranteed.

Each released version follows the format of `<exporter semver>-<kubernetes version>`, where exporter semver is the version of the exporter and the kubernetes version suffix indicates the Kubernetes client version with which this release was built. Compatibility can be expected within the standard Kubernetes compatibility skew (currently 1 minor version on both sides).
